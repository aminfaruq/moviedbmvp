package id.co.maminfaruq.moviecatalogue.network;

import id.co.maminfaruq.moviecatalogue.BuildConfig;
import id.co.maminfaruq.moviecatalogue.model.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    //Movie
    @GET("/3/discover/movie?api_key=" + BuildConfig.TMDB_API_KEY + "&language=en-US")
    Call<MovieResponse> getMovie();

    //search movie
    @GET("/3/search/movie?api_key="+ BuildConfig.TMDB_API_KEY + "&language=en-US")
    Call<MovieResponse>searchMovie(
            @Query("query")String namaMovie
    );

    //tvShow
    @GET("/3/discover/tv?api_key=" + BuildConfig.TMDB_API_KEY + "&language=en-US")
    Call<MovieResponse>getTvShow();

    //search tv
    @GET("/3/search/tv?api_key=" +  BuildConfig.TMDB_API_KEY +"&language=en-US")
    Call<MovieResponse>searchTv(
            @Query("query")String namaTv
    );
}
