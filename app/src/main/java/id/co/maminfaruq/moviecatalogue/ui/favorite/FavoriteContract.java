package id.co.maminfaruq.moviecatalogue.ui.favorite;

import android.content.Context;

import java.util.List;

import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

public interface FavoriteContract {

    interface View {
        void hideRefresh();

        void showDataList(List<ItemMovie> itemMovieList);

        void showFailureMessage(String msg);
    }

    interface Presenter {
        void getDataListMovie(Context context);

        void searchTeams(Context context, String searchText);
    }
}
