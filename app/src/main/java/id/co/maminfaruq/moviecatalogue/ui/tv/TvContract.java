package id.co.maminfaruq.moviecatalogue.ui.tv;

import java.util.List;

import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

public interface TvContract {

    interface View {
        void showProgress();

        void hideProgress();

        void showDataList(List<ItemMovie> resultsItems);

        void showFailureMessage(String msg);
    }

    interface Presenter {
        void getDataListTv();

        void searchTv(final String text);
    }
}
