package id.co.maminfaruq.moviecatalogue.ui.tv;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.co.maminfaruq.moviecatalogue.R;
import id.co.maminfaruq.moviecatalogue.adapter.MovieAdapter;
import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

/**
 * A simple {@link Fragment} subclass.
 */
public class TvFragment extends Fragment implements TvContract.View {


    public static final String KEY_TV = "KEY_TV";
    @BindView(R.id.rv_movie)
    RecyclerView rvMovie;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    private TvPresenter presenter = new TvPresenter(this);


    public TvFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tv, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter.getDataListTv();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.getDataListTv();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getDataListTv();

    }

    @Override
    public void showProgress() {
        refreshLayout.setRefreshing(true);

    }

    @Override
    public void hideProgress() {
        refreshLayout.setRefreshing(false);

    }

    @Override
    public void showDataList(List<ItemMovie> resultsItems) {
        rvMovie.setLayoutManager(new LinearLayoutManager(getContext()));
        rvMovie.setAdapter(new MovieAdapter(getContext(), resultsItems));
    }

    @Override
    public void showFailureMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
