package id.co.maminfaruq.moviecatalogue.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.Data;


@Entity(tableName = "movie")
public class ItemMovie implements Parcelable {

    @SerializedName("id")
    @PrimaryKey
    @ColumnInfo(name = "id")
    private int id;

    @SerializedName("overview")
    @ColumnInfo(name = "overview")
    private String overview;

    @SerializedName(value = "original_title", alternate = ("original_name"))
    @ColumnInfo(name = "original_title")
    private String originalTitle;

    @SerializedName("backdrop_path")
    @ColumnInfo(name = "backdrop_path")
    private String backdropPath;

    @SerializedName(value = "release_date", alternate = ("first_air_date"))
    @ColumnInfo(name = "releaseDate")
    private String releaseDate;

    public ItemMovie(int id, String overview, String originalTitle, String backdropPath, String releaseDate) {
        this.id = id;
        this.overview = overview;
        this.originalTitle = originalTitle;
        this.backdropPath = backdropPath;
        this.releaseDate = releaseDate;
    }

    public ItemMovie() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.overview);
        dest.writeString(this.originalTitle);
        dest.writeString(this.backdropPath);
        dest.writeString(this.releaseDate);
    }

    protected ItemMovie(Parcel in) {
        this.id = in.readInt();
        this.overview = in.readString();
        this.originalTitle = in.readString();
        this.backdropPath = in.readString();
        this.releaseDate = in.readString();
    }

    public static final Parcelable.Creator<ItemMovie> CREATOR = new Parcelable.Creator<ItemMovie>() {
        @Override
        public ItemMovie createFromParcel(Parcel source) {
            return new ItemMovie(source);
        }

        @Override
        public ItemMovie[] newArray(int size) {
            return new ItemMovie[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public static Creator<ItemMovie> getCREATOR() {
        return CREATOR;
    }
}
