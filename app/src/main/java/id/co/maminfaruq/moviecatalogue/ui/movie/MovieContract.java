package id.co.maminfaruq.moviecatalogue.ui.movie;

import android.content.Context;

import java.util.List;

import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

public interface MovieContract {

    interface View {
        void showProgress();

        void hideProgress();

        void showDataList(List<ItemMovie> itemMovies);

        void showFailureMessage(String msg);
    }

    interface Presenter {
        void getDataListMovie(Context context);

        void searchMovie(String text);
    }
}
