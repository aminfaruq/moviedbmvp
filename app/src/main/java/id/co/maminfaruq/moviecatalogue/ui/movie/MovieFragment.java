package id.co.maminfaruq.moviecatalogue.ui.movie;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import id.co.maminfaruq.moviecatalogue.R;
import id.co.maminfaruq.moviecatalogue.adapter.MovieAdapter;
import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

/**
 * A simple {@link Fragment} subclass.
 */
public class MovieFragment extends Fragment implements MovieContract.View {


    public static final String KEY_MOVIES = "KEY_MOVIES";
    @BindView(R.id.rv_movie)
    RecyclerView rvMovie;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    private MoviePresenter presenter = new MoviePresenter(this);


    public MovieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        presenter.getDataListMovie(getActivity());

        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                presenter.getDataListMovie(getActivity());
            }
        });
    }

    @Override
    public void showProgress() {
        refreshLayout.setRefreshing(true);

    }

    @Override
    public void hideProgress() {
        refreshLayout.setRefreshing(false);

    }

    @Override
    public void showDataList(List<ItemMovie> itemMovies) {
        rvMovie.setHasFixedSize(true);
        rvMovie.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvMovie.setAdapter(new MovieAdapter(getActivity(), itemMovies));

    }

    @Override
    public void showFailureMessage(String msg) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

}
