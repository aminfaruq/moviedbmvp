package id.co.maminfaruq.moviecatalogue.network;

import id.co.maminfaruq.moviecatalogue.utils.MyKey;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit getClient() {

        return new Retrofit.Builder()
                .baseUrl(MyKey.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }
}
