package id.co.maminfaruq.moviecatalogue.ui.detail;

import android.content.Context;
import android.os.Bundle;

import id.co.maminfaruq.moviecatalogue.database.MovieDatabase;
import id.co.maminfaruq.moviecatalogue.model.ItemMovie;
import id.co.maminfaruq.moviecatalogue.utils.MyKey;

public class DetailPresenter implements DetailContract.Presenter {

    private final DetailContract.View view;
    private ItemMovie itemMovie;
    private MovieDatabase movieDatabase;

    public DetailPresenter(DetailContract.View view) {
        this.view = view;
    }

    @Override
    public void getDetailMovie(Bundle bundle) {
        if (bundle != null) {
            itemMovie = (ItemMovie) bundle.getSerializable(MyKey.KEY_DATA);
            view.showDetailMovie(itemMovie);
        } else {
            view.showFailureMessage("Data kosong");
        }
    }

    @Override
    public void addToFavorite(Context context, ItemMovie itemMovie) {
        movieDatabase = MovieDatabase.getMovieDatabase(context);
        movieDatabase.movieDao().insertItem(itemMovie);
        view.showSuccessMessage("Tersimpan");
    }

    @Override
    public void removeFavorite(Context context, ItemMovie itemMovie) {
        movieDatabase = MovieDatabase.getMovieDatabase(context);
        movieDatabase.movieDao().delete(itemMovie);

    }

    @Override
    public Boolean checkFavorite(Context context, ItemMovie itemMovie) {
        Boolean bFavorite = false;
        movieDatabase = MovieDatabase.getMovieDatabase(context);
        bFavorite = movieDatabase.movieDao().selectItem(itemMovie.getId()) != null;
        return bFavorite;
    }
}
