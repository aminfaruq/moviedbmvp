package id.co.maminfaruq.moviecatalogue.ui.detail;

import android.content.Context;
import android.os.Bundle;

import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

public interface DetailContract {

    interface View {
        void showDetailMovie(ItemMovie itemMovie);

        void showFailureMessage(String msg);

        void showSuccessMessage(String msg);
    }

    interface Presenter {
        void getDetailMovie(Bundle bundle);

        void addToFavorite(Context context, ItemMovie itemMovie);

        void removeFavorite(Context context, ItemMovie itemMovie);

        Boolean checkFavorite(Context context, ItemMovie itemMovie);
    }
}
