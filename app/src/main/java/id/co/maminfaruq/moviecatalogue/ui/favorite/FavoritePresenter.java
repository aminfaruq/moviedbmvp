package id.co.maminfaruq.moviecatalogue.ui.favorite;

import android.content.Context;

import id.co.maminfaruq.moviecatalogue.database.MovieDatabase;

public class FavoritePresenter implements FavoriteContract.Presenter {

    private FavoriteContract.View view;
    private MovieDatabase movieDatabase;

    public FavoritePresenter(FavoriteContract.View view) {
        this.view = view;
    }

    @Override
    public void getDataListMovie(Context context) {
        movieDatabase = MovieDatabase.getMovieDatabase(context);
        if (movieDatabase.movieDao().selectFavorite() != null){
            view.showDataList(movieDatabase.movieDao().selectFavorite());
        }else {
            view.showFailureMessage("Tidak ada data");
        }
        view.hideRefresh();
    }

    @Override
    public void searchTeams(Context context, String searchText) {

    }
}
