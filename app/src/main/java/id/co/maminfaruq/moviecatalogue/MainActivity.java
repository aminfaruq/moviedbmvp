package id.co.maminfaruq.moviecatalogue;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import id.co.maminfaruq.moviecatalogue.ui.favorite.FavoriteFragment;
import id.co.maminfaruq.moviecatalogue.ui.movie.MovieFragment;
import id.co.maminfaruq.moviecatalogue.ui.tv.TvFragment;

public class MainActivity extends AppCompatActivity {

    private MovieFragment movieFragment;
    private FavoriteFragment favoriteFragment;
    private TvFragment tvFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            movieFragment = new MovieFragment();
            tvFragment = new TvFragment();
            favoriteFragment = new FavoriteFragment();

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, movieFragment)
                    .addToBackStack("frag")
                    .commit();
        } else {
            movieFragment = (MovieFragment) getSupportFragmentManager().findFragmentByTag("frag");
            tvFragment = (TvFragment) getSupportFragmentManager().findFragmentByTag("frag");
            favoriteFragment = (FavoriteFragment) getSupportFragmentManager().findFragmentByTag("frag");
        }

        setTitle("Movie Catalogue");

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment mFragment = null;
            Bundle bundle = new Bundle();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mFragment = new MovieFragment();
                    bundle.putString(MovieFragment.KEY_MOVIES, "MOVIE");
                    mFragment.setArguments(bundle);
                    break;
                case R.id.navigation_dashboard:
                    mFragment = new TvFragment();
                    bundle.putString(TvFragment.KEY_TV,"TV");
                    mFragment.setArguments(bundle);
                    break;
                case R.id.navigation_notifications:
                    mFragment = new FavoriteFragment();
                    bundle.putString(FavoriteFragment.KEY_FAVORITE,"FAVORITE");
                    mFragment.setArguments(bundle);
                    break;
            }
            if (mFragment != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.frame_container, mFragment)
                        .addToBackStack("frag")
                        .commit();

            }
            return true;
        }
    };

}
