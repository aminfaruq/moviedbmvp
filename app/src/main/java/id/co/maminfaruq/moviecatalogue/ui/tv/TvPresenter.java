package id.co.maminfaruq.moviecatalogue.ui.tv;

import id.co.maminfaruq.moviecatalogue.model.MovieResponse;
import id.co.maminfaruq.moviecatalogue.network.ApiClient;
import id.co.maminfaruq.moviecatalogue.network.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvPresenter implements TvContract.Presenter {

    private final TvContract.View view;

    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);

    public TvPresenter(TvContract.View view) {
        this.view = view;
    }


    @Override
    public void getDataListTv() {
        view.showProgress();

        Call<MovieResponse> call = apiInterface.getTvShow();

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                view.hideProgress();
                if (response.isSuccessful()){
                    view.showDataList(response.body().getResults());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                view.hideProgress();
                view.showFailureMessage(t.getMessage());
            }
        });
    }

    @Override
    public void searchTv(String text) {

    }
}
