package id.co.maminfaruq.moviecatalogue.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

@Dao
public interface MovieDao {

    @Insert
    void insertItem(ItemMovie teamsItemList);

    @Query("SELECT * FROM movie WHERE id = :id")
    ItemMovie selectItem(int id);

    @Delete
    void delete(ItemMovie itemMovie);

    @Query("SELECT * FROM movie ORDER BY original_title ASC")
    List<ItemMovie> selectFavorite();
}
