package id.co.maminfaruq.moviecatalogue.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

public class MovieResponse {
    @SerializedName("results")
    private List<ItemMovie> results;

    public List<ItemMovie> getResults() {
        return results;
    }

    public void setResults(List<ItemMovie> results) {
        this.results = results;
    }
}
