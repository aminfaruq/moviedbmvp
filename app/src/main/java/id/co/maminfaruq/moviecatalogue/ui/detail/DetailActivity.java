package id.co.maminfaruq.moviecatalogue.ui.detail;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.maminfaruq.moviecatalogue.R;
import id.co.maminfaruq.moviecatalogue.model.ItemMovie;

public class DetailActivity extends AppCompatActivity implements DetailContract.View{

    public static final String EXTRA_OBJ = "obj";
    @BindView(R.id.ivMovie)
    ImageView ivMovie;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvRelease)
    TextView tvRelease;
    @BindView(R.id.tvContent)
    TextView tvContent;

    private DetailPresenter detailPresenter = new DetailPresenter(this);
    private Menu menu;
    private ItemMovie itemMovie;
    private Boolean isFavorite = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        //Bundle bundle = getIntent().getExtras();
        //detailPresenter.getDetailMovie(bundle);

        setData();
    }

    private void setData() {
        itemMovie = getIntent().getParcelableExtra(EXTRA_OBJ);
        String judul = itemMovie.getOriginalTitle();
        String gambar = itemMovie.getBackdropPath();
        String content = itemMovie.getOverview();
        String rilis = itemMovie.getReleaseDate();
        int id = itemMovie.getId();

        itemMovie.setId(id);
        tvTitle.setText(judul);
        tvRelease.setText(rilis);
        tvContent.setText(content);
        setTitle(judul);

        Glide.with(this).load("https://image.tmdb.org/t/p/original" + gambar).into(ivMovie);
        isFavorite = detailPresenter.checkFavorite(this, itemMovie);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.favorite, menu);
        setFavorite();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_favorite:
                if (isFavorite) {
                    detailPresenter.removeFavorite(this, itemMovie);
                    finish();

                } else {
                    detailPresenter.addToFavorite(this, itemMovie);
                }
                isFavorite = detailPresenter.checkFavorite(this, itemMovie);
                setFavorite();
                break;
            case android.R.id.home: {
                finish();
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                break;
            }
        }
        return true;
    }

    @Override
    public void showDetailMovie(ItemMovie itemMovie) {

        this.itemMovie = itemMovie;
        Glide.with(this).load(itemMovie.getBackdropPath()).into(ivMovie);
        tvTitle.setText(itemMovie.getOriginalTitle());
        tvContent.setText(itemMovie.getOverview());

    }

    @Override
    public void showFailureMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSuccessMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void setFavorite() {
        if (isFavorite) {
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
        } else {
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
        }
    }
}
