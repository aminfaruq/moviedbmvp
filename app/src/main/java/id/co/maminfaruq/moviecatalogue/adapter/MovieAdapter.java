package id.co.maminfaruq.moviecatalogue.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.maminfaruq.moviecatalogue.R;
import id.co.maminfaruq.moviecatalogue.model.ItemMovie;
import id.co.maminfaruq.moviecatalogue.ui.detail.DetailActivity;
import id.co.maminfaruq.moviecatalogue.utils.MyKey;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {

    private Context context;
    private List<ItemMovie> list;

    public MovieAdapter(Context context, List<ItemMovie> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_row, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        final ItemMovie itemMovie = list.get(i);

        viewHolder.tvTitle.setText(itemMovie.getOriginalTitle());
        viewHolder.tvContent.setText(itemMovie.getOverview());

        final String urlImage = "https://image.tmdb.org/t/p/original" + list.get(i).getBackdropPath();
        Glide.with(context).load(urlImage).into(viewHolder.imgNowPlaying);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ItemMovie itemList = new ItemMovie();

                itemList.setOriginalTitle(list.get(i).getOriginalTitle());
                itemList.setBackdropPath(list.get(i).getBackdropPath());
                itemList.setOverview(list.get(i).getOverview());
                itemList.setReleaseDate(list.get(i).getReleaseDate());
                itemList.setId(list.get(i).getId());

                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra(DetailActivity.EXTRA_OBJ,itemList);
                context.startActivity(intent);

                ((Activity) context).overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.imgNowPlaying)
        ImageView imgNowPlaying;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvContent)
        TextView tvContent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
