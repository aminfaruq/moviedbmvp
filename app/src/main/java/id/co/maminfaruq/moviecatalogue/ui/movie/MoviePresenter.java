package id.co.maminfaruq.moviecatalogue.ui.movie;

import android.content.Context;

import id.co.maminfaruq.moviecatalogue.model.MovieResponse;
import id.co.maminfaruq.moviecatalogue.network.ApiClient;
import id.co.maminfaruq.moviecatalogue.network.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoviePresenter implements MovieContract.Presenter {

    private final MovieContract.View view;
    private ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);


    public MoviePresenter(MovieContract.View view) {
        this.view = view;
    }


    @Override
    public void getDataListMovie(Context context) {

        view.showProgress();

        Call<MovieResponse> call = apiInterface.getMovie();

        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.body() != null) {
                    view.hideProgress();
                    view.showDataList(response.body().getResults());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                view.hideProgress();
            }
        });

    }

    @Override
    public void searchMovie(String text) {

    }
}
